﻿namespace PesquisaBinariaExercicios
{
    public static class PesquisaBinariaBasica
    {
        public static int? PesquisaBinariaEmListaOrdenada(int[] lista, int item, out int passos)
        {
            passos = 0;
            int baixo = 0, alto = lista.Length - 1;

            while (baixo <= alto)
            {
                passos++;
                int meio = (baixo + alto) / 2;
                int chute = lista[meio];

                if (chute == item)
                    return meio;

                if (chute > item)
                {
                    alto = meio - 1;
                }
                else
                {
                    baixo = meio + 1;
                }
            }
            return null;
        }
    }
}
