using PesquisaBinariaExercicios;

namespace PesquisaBinariaTestes
{
    public class PesquisaBinariaBasicaTestes
    {

        protected int[] ObterListaOrdenada(int minRange, int maxRange)
        {

            Random rand = new Random();
            int[] lista = Enumerable.Range(minRange, maxRange).OrderBy(x => rand.Next()).ToArray();

            // Ordena a lista para a pesquisa bin�ria
            Array.Sort(lista);

            return lista;
        }
        protected int ObterItemAleatorioDaLista(int[] lista)
        {
            Random rand = new Random();
            return lista[rand.Next(lista.Length)];
        }

        [Fact]
        public void PesquisaBinariaEmListaOrdenada_DeveRetornarResultado_NaoNulo_Quando_Item_Esta_Na_Lista()
        {
            var lista = ObterListaOrdenada(1, 100);
            int item = ObterItemAleatorioDaLista(lista);
            int? resultado = PesquisaBinariaBasica.PesquisaBinariaEmListaOrdenada(lista, item, out int passos);
            Assert.NotNull(resultado);
            Assert.Equal(Array.IndexOf(lista, item), resultado);
        }

        [Fact]
        public void PesquisaBinariaEmListaOrdenada_DeveRetornarResultado_Nulo_Quando_Item_NaoEsta_Na_Lista()
        {
            var lista = ObterListaOrdenada(1, 150);
            int item = 151;
            int? resultado = PesquisaBinariaBasica.PesquisaBinariaEmListaOrdenada(lista, item, out int passos);
            Assert.Null(resultado);
           
        }

        [Fact]
        public void PesquisaBinariaEmListaOrdenada_Os_Passos_DeveSer_Menor_Ou_Igual_Pior_Caso_Da_Pesquisa()
        {
            var lista = ObterListaOrdenada(1, 10000);
            int item = 10000;
            int maxPassos = (int)Math.Ceiling(Math.Log2(lista.Length));
            int? resultado = PesquisaBinariaBasica.PesquisaBinariaEmListaOrdenada(lista, item, out int passos);
            Assert.True(maxPassos >= passos);

        }

        [Fact]
        public void PesquisaBinariaEmListaOrdenada_Os_Passos_DeveSer_Menor_Ou_Igual_Pior_Caso_Da_Pesquisa_Quando_Item_NaoEsta_Na_lista()
        {
            var lista = ObterListaOrdenada(1, 10000);
            int item = 10000 + 1;
            int maxPassos = (int)Math.Ceiling(Math.Log2(lista.Length));
            int? resultado = PesquisaBinariaBasica.PesquisaBinariaEmListaOrdenada(lista, item, out int passos);
            Assert.True(maxPassos >= passos);

        }
    }
}